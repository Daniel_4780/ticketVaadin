package com.example;

import javax.servlet.annotation.WebServlet;
import javax.xml.soap.Text;

import clases.Escenario;
import com.sun.org.apache.regexp.internal.RE;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import clases.*;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        Sistema sistema = new Sistema();
        List<Escenario> listadoEscenario = new ArrayList<>();
        List<DatosPrueba> listadoDatosPrueba = new ArrayList<>();

        VerticalLayout cuerpo = new VerticalLayout();
        Button btnCargarDatos = new Button("Cargar datos");
        btnCargarDatos.setStyleName("primary");
        btnCargarDatos.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                CargarDatos cargarDatos = new CargarDatos();
                sistema.setListadoRequerimientos(cargarDatos.getListadoRequerimiento());
                sistema.setListadoTicket(cargarDatos.getListadoTicke());
                Notification.show("Se crearon con exito los datos");
            }
        });
        cuerpo.addComponent(btnCargarDatos);

        //Contenido de crear ticket
        FormLayout  agregarTicket = new FormLayout ();
        HorizontalLayout agregarTicketH = new HorizontalLayout();
        TextField numeroTicket = new TextField("Ingrese numero de tiecket");
        TextField memorandumTicket = new TextField("Ingrese numero de memorandum");
        TextField descripcionTiecket = new TextField("Ingrese descripcion del ticket");
        agregarTicketH.addComponents(numeroTicket, memorandumTicket, descripcionTiecket);

        Button btnAgregarTicket = new Button("Agregar ticket");
        btnAgregarTicket.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Ticket ticket = new Ticket();
                ticket.setTicketId(Integer.parseInt(numeroTicket.getValue()));
                ticket.setMemorandum(memorandumTicket.getValue());
                ticket.setDescripcion(descripcionTiecket.getValue());
                sistema.AgregarTicket(ticket);
                numeroTicket.setValue("");
                memorandumTicket.setValue("");
                descripcionTiecket.setValue("");
                Notification.show("Se agrego con exito el ticket: " + ticket.getTicketId()+ " con memoramdun: " + ticket.getMemorandum());
            }
        });
        btnAgregarTicket.addStyleName("primary");
        agregarTicket.addComponents(agregarTicketH, btnAgregarTicket);

        //Contenido del crear Iteracion
        FormLayout agregarIteracon = new FormLayout();
        HorizontalLayout agregarIteracionH1 = new HorizontalLayout();
        HorizontalLayout agregarIteracionH2 = new HorizontalLayout();
        ComboBox<Ticket> slcNumeroTickeIteracion = new ComboBox<>("Seleccione el numero de ticket");
        Button btnCargarTicketIteracion = new Button("Cargar ticket");
        btnCargarTicketIteracion.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                slcNumeroTickeIteracion.setItems(sistema.getListadoTicket());
                slcNumeroTickeIteracion.setItemCaptionGenerator(Ticket::getMemorandum);
            }
        });
        TextField nombreAnalistaQA = new TextField("Ingrese nombre del analisista de QA");
        TextField nombreAnalistaDesa = new TextField("Ingrese nombre del analista de desarrollo");
        TextField nombreAnalisitaDPI = new TextField("Ingrese nombre del analisita de administrador de proyectos");
        TextField nombreAnalisitaNormativo = new TextField("Ingrese nombre del analisita de normatividad");

        Button btnAgregarIteracion = new Button("Agregar iteracion");
        btnAgregarIteracion.addStyleName("primary");
        btnAgregarIteracion.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Iteracion iteracion = new Iteracion();
                iteracion.setAnalisitaQA(nombreAnalistaQA.getValue());
                iteracion.setAnalistaDesa(nombreAnalistaDesa.getValue());
                iteracion.setAnalistaNormativo(nombreAnalisitaNormativo.getValue());
                iteracion.setAnalistaNormativo(nombreAnalisitaNormativo.getValue());
                Ticket ticket = slcNumeroTickeIteracion.getSelectedItem().get();
                sistema.AgregarIteraccionTicket(slcNumeroTickeIteracion.getTabIndex(), iteracion);
                Notification.show("Se ingreso correctame");
            }
        });

        agregarIteracionH1.addComponents(btnCargarTicketIteracion, slcNumeroTickeIteracion, nombreAnalistaQA);
        agregarIteracionH1.setComponentAlignment(btnCargarTicketIteracion, Alignment.BOTTOM_CENTER);
        agregarIteracionH2.addComponents(nombreAnalistaDesa, nombreAnalisitaDPI, nombreAnalisitaNormativo);
        agregarIteracon.addComponents(agregarIteracionH1, agregarIteracionH2, btnAgregarIteracion);

        //Contenido del crear requerimiento
        FormLayout agregarRequerimiento = new FormLayout();
        HorizontalLayout agregarRequerimientoH = new HorizontalLayout();
        TextField nombreRequerimiento = new TextField("Ingrese nombre del requerimiento");
        TextField versionRequerimiento = new TextField("Ingrese version del requeriemiento");

        TextField descripcionEscenario = new TextField("Ingrese la descripcion del escenario");
        TextField resultadoEscenario = new TextField("Ingrese el resultado del escenario");
        Button btnAgregarEscenario = new Button("Agregar escenrio");
        Grid<Escenario>  griEscenario = new Grid<>();
        griEscenario.setItems(listadoEscenario);
        griEscenario.addColumn(Escenario::getDescripcion).setCaption("Descripcion");
        griEscenario.addColumn(Escenario::getResultadoEsperado).setCaption("Resultado");
        btnAgregarEscenario.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Escenario escenario = new Escenario();
                escenario.setDescripcion(descripcionEscenario.getValue());
                escenario.setResultadoEsperado(resultadoEscenario.getValue());
                listadoEscenario.add(escenario);
                griEscenario.setItems(listadoEscenario);
                descripcionEscenario.setValue("");
                resultadoEscenario.setValue("");
                Notification.show("Se agrego un escenario");
            }
        });
        Button btnAgregarRequerimiento = new Button("Agregar requerimiento");
        btnAgregarRequerimiento.addStyleName("primary");
        btnAgregarRequerimiento.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Requerimiento requerimiento = new Requerimiento();
                requerimiento.setListadoEscenarios(listadoEscenario);
                requerimiento.setNombreRquerimiento(nombreRequerimiento.getValue());
                requerimiento.setVersion(versionRequerimiento.getValue());
                sistema.AgregarRequerimiento(requerimiento);
                nombreRequerimiento.setValue("");
                versionRequerimiento.setValue("");
                listadoEscenario.clear();
                griEscenario.setItems(listadoEscenario);
                Notification.show("Se agrego correctamente el requerimiento: " + requerimiento.getNombreRquerimiento());
            }
        });
        HorizontalLayout agregarRequerimientoH1 = new HorizontalLayout();
        agregarRequerimientoH.addComponents(nombreRequerimiento, versionRequerimiento);
        agregarRequerimientoH1.addComponents(descripcionEscenario, resultadoEscenario, btnAgregarEscenario);
        agregarRequerimiento.addComponents(agregarRequerimientoH, agregarRequerimientoH1, griEscenario, btnAgregarRequerimiento);

        //Contenido crear prueba
        FormLayout agregarPrueba = new FormLayout();
        HorizontalLayout agregarPruebaH1 = new HorizontalLayout();
        HorizontalLayout agregarPruebaH2 = new HorizontalLayout();

        Button btnCargarTicketPreuba = new Button("Cargar ticket");
        Button btnCargarIteracionPreuba = new Button("Cargar iterciones");
        Button btnCargarRequerimientoPrueba = new Button("Cargar requerimiento");
        Button btnCargarEscenarioPrueba = new Button("Cargar escenarios");

        TextField resultadoPrueba = new TextField("Resultado de la prueba");

        ComboBox<Ticket> slcTicket = new ComboBox<>("Seleccione un ticket");
        ComboBox<Iteracion> slcIteraciones = new ComboBox<>("Seleccione una iteracion");
        ComboBox<Requerimiento> slcRequerimiento = new ComboBox<>("Seleccione un requerimiento");
        ComboBox<Escenario> slcEscenario = new ComboBox<>("Seleccione un escenario");
        Button btnCrearPreuba = new Button("Agregar prueba");


        btnCargarTicketPreuba.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                slcTicket.setItems(sistema.getListadoTicket());
                slcTicket.setItemCaptionGenerator(Ticket::getMemorandum);
            }
        });

        btnCargarIteracionPreuba.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Ticket ticket = sistema.getListadoTicket().get(slcTicket.getTabIndex());
                slcIteraciones.setItems(ticket.getListadoIteraciones());
                slcIteraciones.setItemCaptionGenerator(Iteracion::getAnalisitaQA);
            }
        });

        btnCargarRequerimientoPrueba.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                slcRequerimiento.setItems(sistema.getListadoRequerimientos());
                slcRequerimiento.setItemCaptionGenerator(Requerimiento::getNombreRquerimiento);
            }
        });

        btnCargarEscenarioPrueba.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Requerimiento requerimiento = sistema.getListadoRequerimientos().get(slcRequerimiento.getTabIndex());
                slcEscenario.setItems(requerimiento.getListadoEscenarios());
                slcEscenario.setItemCaptionGenerator(Escenario::getDescripcion);
            }
        });

        btnCrearPreuba.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Prueba prueba = new Prueba();
                prueba.setIndiceTicket(slcTicket.getTabIndex());
                prueba.setIndiceIteracion(slcIteraciones.getTabIndex());
                prueba.setIndiceRequerimiento(slcRequerimiento.getTabIndex());
                prueba.setIndiceEscenario(slcEscenario.getTabIndex());
                prueba.setResultado(resultadoPrueba.getValue());
                sistema.AgregarPruebaTicket(prueba);
                slcTicket.clear();
                slcIteraciones.clear();
                slcRequerimiento.clear();
                slcEscenario.clear();
                resultadoPrueba.setValue("");
                Notification.show("Se agrego exitosamente la prueba con resultado: " + prueba.getResultado());
            }
        });
        agregarPruebaH1.addComponents(btnCargarTicketPreuba, slcTicket, btnCargarIteracionPreuba, slcIteraciones);
        agregarPruebaH2.addComponents(btnCargarRequerimientoPrueba, slcRequerimiento, btnCargarEscenarioPrueba, slcEscenario);
        agregarPrueba.addComponents(agregarPruebaH1, agregarPruebaH2, resultadoPrueba, btnCrearPreuba);
        agregarPruebaH1.setComponentAlignment(btnCargarTicketPreuba, Alignment.BOTTOM_CENTER);
        agregarPruebaH1.setComponentAlignment(btnCargarIteracionPreuba, Alignment.BOTTOM_CENTER);
        agregarPruebaH2.setComponentAlignment(btnCargarRequerimientoPrueba, Alignment.BOTTOM_CENTER);
        agregarPruebaH2.setComponentAlignment(btnCargarEscenarioPrueba, Alignment.BOTTOM_CENTER);

        //Mostrar ticket
        FormLayout mostrarTicket = new FormLayout();
        Label tituloTicket = new Label("En la siguiente seccion, se puede observar todos los ticket ingresados");
        Button btnMostrarTicket = new Button("Mostrar tickets");
        Grid<Ticket> gridTicket = new Grid<>();
        mostrarTicket.addComponents(tituloTicket, btnMostrarTicket, gridTicket);
        gridTicket.setItems(sistema.getListadoTicket());
        gridTicket.addColumn(Ticket::getTicketId).setCaption("Numero ticket");
        gridTicket.addColumn(Ticket::getMemorandum).setCaption("Memorandum");
        gridTicket.addColumn(Ticket::getDescripcion).setCaption("Descripcion");
        btnMostrarTicket.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                gridTicket.setItems(sistema.getListadoTicket());
                Notification.show("Se mostraron correctamente los ticket");
            }
        });

        //Mostrar todas las pruebas
        FormLayout mostrarTodosLasPruebas = new FormLayout();
        Label tituloPruebas = new Label("En la siguiente seccion, se puede observar todas las pruebas realizadas");
        Button btnMostrarPruebas = new Button("Mostrar pruebas");
        Grid<DatosPrueba> gridTicketPruebas = new Grid<>();
        gridTicketPruebas.addColumn(DatosPrueba::getNumeroTicket).setCaption("Numero de ticket");
        gridTicketPruebas.addColumn(DatosPrueba::getNombreAnalisita).setCaption("Nombre analisita");
        gridTicketPruebas.addColumn(DatosPrueba::getNombreRequerimiento).setCaption("Nombre requerimiento");
        gridTicketPruebas.addColumn(DatosPrueba::getVersionRequerimiento).setCaption("Version");
        gridTicketPruebas.addColumn(DatosPrueba::getResultadoPreuba).setCaption("Resultado de la prueba");
        btnMostrarPruebas.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                listadoDatosPrueba.clear();
                List<Ticket> listadoTicket = sistema.getListadoTicket();
                for (Ticket t:listadoTicket
                        ) {
                    int numeroTicket = t.getTicketId();
                    String memorandum = t.getMemorandum();
                    if(!t.getListadoPruebas().isEmpty()){
                        for (Prueba p: t.getListadoPruebas()
                                ) {
                            Ticket ticket = listadoTicket.get(p.getIndiceTicket());
                            Iteracion iteracion = ticket.getListadoIteraciones().get(p.getIndiceIteracion());
                            List<Requerimiento> listadoRequerimiento = sistema.getListadoRequerimientos();
                            Requerimiento requerimiento = listadoRequerimiento.get(p.getIndiceRequerimiento());
                            Escenario escenario = requerimiento.getListadoEscenarios().get(p.getIndiceEscenario());

                            DatosPrueba datosPrueba = new DatosPrueba(ticket.getTicketId(), iteracion.getAnalisitaQA(), requerimiento.getNombreRquerimiento(), requerimiento.getVersion(), p.getResultado());
                            listadoDatosPrueba.add(datosPrueba);
                        }
                    }
                }
                gridTicketPruebas.setItems(listadoDatosPrueba);
                Notification.show("Se mostraron la pruebas correctamente");
            }
        });
        mostrarTodosLasPruebas.addComponents(tituloPruebas, btnMostrarPruebas, gridTicketPruebas);
        mostrarTodosLasPruebas.setWidth("100%");

        //Crear arcondion
        Accordion accordion = new Accordion();
        Layout tabTicket = new VerticalLayout();
        Layout tabIteracion = new VerticalLayout();
        Layout tabRequerimiento = new VerticalLayout();
        Layout tabPrueba = new VerticalLayout();
        Layout tabMostrarTicket = new VerticalLayout();
        Layout tabMostrarPruebas = new VerticalLayout();
        tabTicket.addComponent(agregarTicket);
        tabIteracion.addComponent(agregarIteracon);
        tabRequerimiento.addComponents(agregarRequerimiento);
        tabPrueba.addComponents(agregarPrueba);
        tabMostrarTicket.addComponents(mostrarTicket);
        tabMostrarPruebas.addComponents(mostrarTodosLasPruebas);
        accordion.addTab(tabTicket, "Ingrese ticket");
        accordion.addTab(tabIteracion, "Ingresar itercacion");
        accordion.addTab(tabRequerimiento, "Ingresar requerimiento");
        accordion.addTab(tabPrueba, "Ingresar prueba");
        accordion.addTab(tabMostrarTicket, "Mostrar todos los ticke");
        accordion.addTab(tabMostrarPruebas, "Mostrar todas las pruebas");
        cuerpo.addComponent(accordion);
        setContent(cuerpo);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
