package clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PROGRA on 05/07/2017.
 */
public class Sistema {
    private List<Ticket> listadoTicket;
    private List<Requerimiento> listadoRequerimientos;

    public List<Ticket> getListadoTicket() {
        return listadoTicket;
    }

    public void setListadoTicket(List<Ticket> listadoTicket) {
        this.listadoTicket = listadoTicket;
    }

    public List<Requerimiento> getListadoRequerimientos() {
        return listadoRequerimientos;
    }

    public void setListadoRequerimientos(List<Requerimiento> listadoRequerimientos) {
        this.listadoRequerimientos = listadoRequerimientos;
    }

    public Sistema() {
        listadoTicket = new ArrayList<>();
        listadoRequerimientos = new ArrayList<>();
    }

    public Sistema(List<Ticket> listadoTicket, List<Requerimiento> listadoRequerimientos) {
        this.listadoTicket = listadoTicket;
        this.listadoRequerimientos = listadoRequerimientos;
    }

    public void AgregarTicket(Ticket ticket){
        listadoTicket.add(ticket);
    }

    public void AgregarRequerimiento(Requerimiento requerimiento){
        listadoRequerimientos.add(requerimiento);
    }

    public int AgregarIteraccionTicket(int numeroTicket, Iteracion iteracion){
        numeroTicket = numeroTicket;
        Ticket ticket = listadoTicket.get(numeroTicket);
        ticket.AgregarIteraccion(iteracion);
        listadoTicket.set(numeroTicket, ticket);
        return ticket.getTicketId();
    }

    public void AgregarPruebaTicket(Prueba prueba)
    {
        int indiceTicket = prueba.getIndiceTicket();
        Ticket ticket = listadoTicket.get(indiceTicket);
        ticket.AgregarPrueba(prueba);
        listadoTicket.set(indiceTicket, ticket);
    }

    public void MostrarTickets(){
        int contar = 1;
        for (Ticket t:listadoTicket
                ) {
            System.out.println(contar+" Numero de ticket: " + t.getTicketId() + ", memorandum: " + t.getMemorandum());
            contar++;
        }
    }

    public String MostrarTicketsVisual(){
        int contar = 1;
        String mostrarTicket = "";
        for (Ticket t:listadoTicket
                ) {
            mostrarTicket = mostrarTicket + contar+" Numero de ticket: " + t.getTicketId() + ", memorandum: " + t.getMemorandum()+ "\n";
            contar++;
        }
        return  mostrarTicket;
    }

    public void MostrarRequerimientos(){
        int contar = 1;
        for (Requerimiento r:listadoRequerimientos
                ) {
            System.out.println(contar + " Nombre del requerimiento: " + r.getNombreRquerimiento() + ", version: " + r.getVersion());
            contar++;
        }
    }

    public String MostrarRequerimientosVisual(){
        int contar = 1;
        String menu = "";
        for (Requerimiento r:listadoRequerimientos
                ) {
            menu = menu + contar + " Nombre del requerimiento: " + r.getNombreRquerimiento() + ", version: " + r.getVersion() + "\n";
            contar++;
        }
        return menu;
    }

    public boolean MostrarIteraciones(int valorIndice){
        if(!listadoTicket.isEmpty()){
            Ticket ticket = listadoTicket.get(valorIndice);
            int contar = 1;
            for (Iteracion i: ticket.getListadoIteraciones()
                    ) {
                System.out.println(contar + " Numero e iteracion");
                contar++;
            }
            return true;
        }
        else{
            System.out.println("Ingrese al una iteracion.");
            return false;
        }
    }

    public String MostrarIteracionesVisual(int valorIndice){
        String menu = "";
        if(!listadoTicket.isEmpty()){
            Ticket ticket = listadoTicket.get(valorIndice);
            int contar = 1;
            for (Iteracion i: ticket.getListadoIteraciones()
                    ) {
                menu = menu + contar + " Numero e iteracion\n";
                contar++;
            }
        }
        else{
            menu = "Ingrese al una iteracion.";
        }
        return menu;
    }


    public void MostrarEscenarios(int valorIndice){
        Requerimiento requerimiento = listadoRequerimientos.get(valorIndice);
        int contar = 1;
        for (Escenario e: requerimiento.getListadoEscenarios()
                ) {
            System.out.println(contar + " Descripcion: " + e.getDescripcion() + ", Resultado esperado: " + e.getResultadoEsperado());
            contar++;
        }
    }

    public String MostrarEscenariosVisual(int valorIndice){
        Requerimiento requerimiento = listadoRequerimientos.get(valorIndice);
        String menu = "";
        int contar = 1;
        for (Escenario e: requerimiento.getListadoEscenarios()
                ) {
            menu = menu + contar + " Descripcion: " + e.getDescripcion() + ", Resultado esperado: " + e.getResultadoEsperado() + "\n";
            contar++;
        }
        return menu;
    }

    public void MostrarTicketIteracion(){
        for (Ticket t:listadoTicket
             ) {
            System.out.println("Numero de ticket: " + t.getTicketId() + ", memorandum: " + t.getMemorandum());
            if (!t.getListadoIteraciones().isEmpty()){
                int contar = 1;
                for (Iteracion i:t.getListadoIteraciones()
                        ) {
                    System.out.println(contar + " Numero de iteracion: " + contar);
                    contar++;
                }
            }
        }
    }

    public void MostrarRequerimientosEscenario(){
        for (Requerimiento r:listadoRequerimientos
             ) {
            System.out.println("Nombre del requerimiento: " + r.getNombreRquerimiento() + ", version: " + r.getVersion());
            if(!r.getListadoEscenarios().isEmpty()){
                int contar = 1;
                for (Escenario e: r.getListadoEscenarios()){
                    System.out.println(contar + " Descripcion: " + e.getDescripcion() + ", Resultado esperado: " + e.getResultadoEsperado());
                    contar++;
                }
            }
        }
    }

    public String MostrarRequerimientosEscenarioVisual(){
        String menu = "";
        for (Requerimiento r:listadoRequerimientos
                ) {
            menu = menu + "Nombre del requerimiento: " + r.getNombreRquerimiento() + ", version: " + r.getVersion() + "\n";
            if(!r.getListadoEscenarios().isEmpty()){
                int contar = 1;
                for (Escenario e: r.getListadoEscenarios()){
                    menu = menu + contar + " Descripcion: " + e.getDescripcion() + ", Resultado esperado: " + e.getResultadoEsperado() + "\n";
                    contar++;
                }
            }
        }
        return menu;
    }

    public void MostrarPruebasTicket(int numeroTicket){
        for (Ticket t:listadoTicket
                ) {
            System.out.println("Numero de ticket: " + t.getTicketId() + ", memorandum: " + t.getMemorandum());
            if (!t.getListadoIteraciones().isEmpty()){
                int contar = 1;
                for (Iteracion i:t.getListadoIteraciones()
                        ) {
                    System.out.println("Numero e iteracion: " + contar);
                    contar++;
                }
            }
        }
    }

    public void MostrarTodasPruebas(){
        for (Ticket t:listadoTicket
                ) {
            System.out.println("Numero de ticket: " + t.getTicketId() + ", memorandum: " + t.getMemorandum());
            if(!t.getListadoPruebas().isEmpty()){
                int contar = 1;
                for (Prueba p: t.getListadoPruebas()
                     ) {
                    Ticket ticket = listadoTicket.get(p.getIndiceTicket());
                    Iteracion iteracion = ticket.getListadoIteraciones().get(p.getIndiceIteracion());
                    Requerimiento requerimiento = listadoRequerimientos.get(p.getIndiceRequerimiento());
                    Escenario escenario = requerimiento.getListadoEscenarios().get(p.getIndiceEscenario());
                    System.out.println(contar + " Numero de ticket:" + ticket.getTicketId() +
                            ", Analista de desarrollo: " + iteracion.getAnalistaDesa() +
                            ", Nombre del requerimiento: " + requerimiento.getNombreRquerimiento() +
                            " y version: " + requerimiento.getVersion() +
                            " con resultado de la prueba: " + p.getResultado());
                    contar++;
                }
            }
        }
    }
}
