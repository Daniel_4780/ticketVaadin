package clases;

/**
 * Created by PROGRA on 04/07/2017.
 */
public class Escenario {
    private String descripcion;
    private String resultadoEsperado;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getResultadoEsperado() {
        return resultadoEsperado;
    }

    public void setResultadoEsperado(String resultadoEsperado) {
        this.resultadoEsperado = resultadoEsperado;
    }

    public Escenario() {
    }

    public Escenario(String descripcion, String resultadoEsperado) {
        this.descripcion = descripcion;
        this.resultadoEsperado = resultadoEsperado;
    }
}
