package clases;

/**
 * Created by PROGRA on 04/07/2017.
 */
public class Prueba {
    private String Resultado;
    private int IndiceTicket;
    private int IndiceIteracion;
    private int IndiceRequerimiento;
    private int IndiceEscenario;

    public String getResultado() {
        return Resultado;
    }

    public void setResultado(String resultado) {
        Resultado = resultado;
    }

    public int getIndiceTicket() {
        return IndiceTicket;
    }

    public void setIndiceTicket(int indiceTicket) {
        IndiceTicket = indiceTicket;
    }

    public int getIndiceIteracion() {
        return IndiceIteracion;
    }

    public void setIndiceIteracion(int indiceIteracion) {
        IndiceIteracion = indiceIteracion;
    }

    public int getIndiceRequerimiento() {
        return IndiceRequerimiento;
    }

    public void setIndiceRequerimiento(int indiceRequerimiento) {
        IndiceRequerimiento = indiceRequerimiento;
    }

    public int getIndiceEscenario() {
        return IndiceEscenario;
    }

    public void setIndiceEscenario(int indiceEscenario) {
        IndiceEscenario = indiceEscenario;
    }

    public Prueba() {
    }

    public Prueba(String resultado, int indiceTicket, int indiceIteracion, int indiceRequerimiento, int indiceEscenario) {
        Resultado = resultado;
        IndiceTicket = indiceTicket;
        IndiceIteracion = indiceIteracion;
        IndiceRequerimiento = indiceRequerimiento;
        IndiceEscenario = indiceEscenario;
    }

}

