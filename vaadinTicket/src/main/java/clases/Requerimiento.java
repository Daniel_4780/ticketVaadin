package clases;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PROGRA on 04/07/2017.
 */
public class Requerimiento {
    private String nombreRquerimiento;
    private String version;
    private List<Escenario> listadoEscenarios;

    public String getNombreRquerimiento() {
        return nombreRquerimiento;
    }

    public void setNombreRquerimiento(String nombreRquerimiento) {
        this.nombreRquerimiento = nombreRquerimiento;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Escenario> getListadoEscenarios() {
        return listadoEscenarios;
    }

    public void setListadoEscenarios(List<Escenario> listadoEscenarios) {
        this.listadoEscenarios = listadoEscenarios;
    }

    public Requerimiento() {
        listadoEscenarios = new ArrayList<>();
    }

    public Requerimiento(String nombreRquerimiento, String version, List<Escenario> listadoEscenarios) {
        this.nombreRquerimiento = nombreRquerimiento;
        this.version = version;
        this.listadoEscenarios = listadoEscenarios;
    }

    public void AgregarEscenario(Escenario escenario){
        listadoEscenarios.add(escenario);
    }
}
