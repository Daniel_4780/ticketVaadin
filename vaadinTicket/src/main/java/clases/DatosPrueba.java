package clases;

/**
 * Created by Proyectos on 20/07/2017.
 */
public class DatosPrueba {
    private int numeroTicket;
    private String nombreAnalisita;
    private String nombreRequerimiento;
    private String versionRequerimiento;
    private String resultadoPreuba;

    public DatosPrueba() {
    }

    public DatosPrueba(int numeroTicket, String nombreAnalisita, String nombreRequerimiento, String versionRequerimiento, String resultadoPreuba) {
        this.numeroTicket = numeroTicket;
        this.nombreAnalisita = nombreAnalisita;
        this.nombreRequerimiento = nombreRequerimiento;
        this.versionRequerimiento = versionRequerimiento;
        this.resultadoPreuba = resultadoPreuba;
    }

    public int getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(int numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    public String getNombreAnalisita() {
        return nombreAnalisita;
    }

    public void setNombreAnalisita(String nombreAnalisita) {
        this.nombreAnalisita = nombreAnalisita;
    }

    public String getNombreRequerimiento() {
        return nombreRequerimiento;
    }

    public void setNombreRequerimiento(String nombreRequerimiento) {
        this.nombreRequerimiento = nombreRequerimiento;
    }

    public String getVersionRequerimiento() {
        return versionRequerimiento;
    }

    public void setVersionRequerimiento(String versionRequerimiento) {
        this.versionRequerimiento = versionRequerimiento;
    }

    public String getResultadoPreuba() {
        return resultadoPreuba;
    }

    public void setResultadoPreuba(String resultadoPreuba) {
        this.resultadoPreuba = resultadoPreuba;
    }
}
